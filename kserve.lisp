(in-package #:kserve)

(defun predict (model-name svc-hostname input-path)
  (let* (
        (endpoint (uiop:getenv "CLUSTER_IP"))
        (command
          (concatenate
            'string
            "curl -v -H \"Host: " svc-hostname "\" "
            "" endpoint "/v1/models/" model-name ":predict"
            " -d " input-path ""
        ))
        (response
            (uiop:run-program command :output :string)))
    (with-input-from-string
      (s response)
      (json:decode-json s))
))
