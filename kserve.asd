(asdf:defsystem #:kserve
  :description "KServe inference client"
  :author "acraze@wikimedia.org"
  :license "GPL 3.0"
  :version  "0.1.0"
  :depends-on (#:cl-json)
  :components ((:file "package")
               (:file "kserve")))
